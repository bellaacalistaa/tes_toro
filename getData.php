<?php 
$conn = array( 
    'host' => 'localhost', 
    'user' => 'root', 
    'pass' => '', 
    'db'   => 'datacustomer' 
); 
 
$dbtable = 'customer'; 
$primaryKey = 'id'; 
$columns = array(  
    array( 'db' => 'nama', 'dt' => 0 ), 
    array( 'db' => 'alamat',  'dt' => 1 ), 
    array( 
        'db'        => 'tanggal_lahir', 
        'dt'        => 2, 
        'formatter' => function( $d, $row ) { 
            return date( 'd M Y', strtotime($d)); 
        } 
    ),
    array(
        'db'        => 'id',
        'dt'        => 3,
        'formatter' => function( $d, $row ) {
            return '<button onclick="updateData('.$d.')" class="btn btn-sm btn-success" id="btnupdate">Update</button> <button onclick="deleteData('.$d.')" class="btn btn-sm btn-danger">Delete</button>';
        }
    )

); 
 
require 'ssp.class.php'; 
echo json_encode( 
    SSP::simple( $_GET, $conn, $dbtable, $primaryKey, $columns ) 
);