-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2022 at 11:40 AM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `datacustomer`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lng` float DEFAULT NULL,
  `id_m_customer_type` int(11) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `nama`, `alamat`, `lat`, `lng`, `id_m_customer_type`, `tanggal_lahir`, `keterangan`, `status`) VALUES
(1, 'aa', 'aa', -6.08192, 109.845, 1, '2022-03-08 17:40:38', 'aa', 1),
(2, 'bb', 'bb', -6.08192, 109.845, 2, '2022-03-02 06:26:43', 'bb', 1),
(3, 'ty', 'tyyy', -6.73933, 109.88, 1, '2022-03-02 15:09:00', 'ty', 0),
(9, 'Bella', 'Bella', NULL, NULL, NULL, '2022-03-09 17:40:25', NULL, NULL),
(10, 'ae', 'ae', NULL, NULL, 2, '2022-03-16 17:40:29', NULL, 1),
(11, 'we', 'we', NULL, NULL, 1, '2022-03-20 17:40:32', NULL, 1),
(13, 'tes', 'tes', NULL, NULL, 1, '2022-03-07 17:40:34', 'tes', 1),
(14, 'al', 'al', NULL, NULL, 1, '2022-03-02 14:32:00', 'al', 1),
(15, 'tr', 'tr', 6.8, 9.7, 1, '2022-03-08 14:35:00', 'tr', 1);

-- --------------------------------------------------------

--
-- Table structure for table `customer_type`
--

CREATE TABLE `customer_type` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `customer_type`
--

INSERT INTO `customer_type` (`id`, `nama`, `alamat`) VALUES
(1, 'Bella', 'Ketintang Madya No.5'),
(2, 'Dinda', 'Wiyung Dinoyo No.21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_customer_has_customer_type` (`id_m_customer_type`);

--
-- Indexes for table `customer_type`
--
ALTER TABLE `customer_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_customer_has_customer_type` FOREIGN KEY (`id_m_customer_type`) REFERENCES `customer_type` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
